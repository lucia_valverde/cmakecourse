#In testAddLibrary
project(testAdd)

add_executable(testAdd addTest.cpp)
target_link_libraries(testAdd PUBLIC addLibrary)

message(AAAA ${BOOST_INCLUDES})
target_include_directories(${PROJECT_NAME} PUBLIC ${BOOST_INCLUDES})

target_link_libraries(${PROJECT_NAME} PUBLIC Boost::unit_test_framework)

add_test(NAME testAdd COMMAND testAdd)
target_include_directories(testAdd PRIVATE .)
