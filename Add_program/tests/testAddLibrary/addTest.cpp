#include "include/libAdd.h"
#include <boost/test/unit_test.hpp>

BOOST_AUTO_TEST_CASE(addTwoNumber_twoIntValues_3){
    int firstValue = 1;
    int secondValue = 2;
    long double expectedValue = 3;
    BOOST_CHECK_EQUAL(libAdd::addTwoNumber(firstValue, secondValue), expectedValue);
}

BOOST_AUTO_TEST_CASE(addTwoNumber_twoDoubleValues_negativeResult){
    double firstValue = -4.3;
    double secondValue = 2.1;
    long double expectedValue = -2.2;
    BOOST_CHECK_EQUAL(libAdd::addTwoNumber(firstValue, secondValue), expectedValue);
}

BOOST_AUTO_TEST_CASE(addTwoNumber_twoBigValues_bigResult){
    long double firstValue = 100000000000000000.1;
    long double secondValue = 300000000000000000.3;
    long double expectedValue = 400000000000000000.4;
    BOOST_CHECK_EQUAL(libAdd::addTwoNumber(firstValue, secondValue), expectedValue);
}

