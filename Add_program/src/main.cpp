#include <iostream>
#include <string>
#include "include/libAdd.h"

using namespace std;

int main(){
    /**
    cout << "Hello! Please write two numbers. The first one: " << endl;

    // long double -> long de punto flotante de doble precision -> 8 bytes 
    long double firstValue;
    long double secondValue;

    cin >> firstValue;

    cout << "The second one: " << endl;
    cin >> secondValue;

    long double currentSum = firstValue + secondValue;
    bool stop;
    
    cout << "The result is: " << currentSum <<". Do you want to add more values? (yes/no)" << endl;

    std::string stopDecision;
    cin >> stopDecision;
    cout << stopDecision << endl;

    //Checking answer fuction?
    if (stopDecision == "yes") {
        stop = false;
    }
    else if (stopDecision == "no") {
        stop = true;
    }
    else {
        cout << "The input values is not valid. The execution ends." << endl;
        stop = true;
    }
    while (!stop){
        cout << "Write the next value: " << endl;
        long double newValue;
        cin >> newValue;

        currentSum = currentSum + newValue;
        cout << "The result is: " << currentSum <<". Do you want to add more values? (yes/no)" << endl;
        cin >> stopDecision;

        if (stopDecision == "yes") {
            stop = false;
        }
        else if (stopDecision == "no") {
            stop = true;
        }
        else {
            cout << "The input values is not valid" << endl;
            stop = true;
        }
    }
    
    cout << "It has been a pleasure, goodbye!" << endl;
    return 0;
    **/

    int firstValue = 5;
    cout << "The first value is: " << firstValue << endl;
    
    double secondValue = 3.5;
    cout << "The second value is: " << secondValue << endl;

    long double result = libAdd::addTwoNumber(firstValue, secondValue);

    cout << "The result is: " << result <<endl; 

    return 0;

}