#ifndef LIBADD_HPP_INCLUDED
#define LIBADD_HPP_INCLUDED

namespace libAdd{
    long double addTwoNumber(long double, long double);
}

#endif