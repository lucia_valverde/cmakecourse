#include "libAdd.h"

long double libAdd::addTwoNumber(long double value1, long double value2){
    long double result = value1 + value2;
    return result;
}